from matplotlib import pyplot as plt
import numpy as np





## Constants
pi = np.pi
phidot = 0. #set follower to have initial angular velocity of 0
t = 0. #initial time, set to 0
deltaphi = 0.00000000000005  #to calculate numerical derivative dU/dphi
energy = 0. #variable to calculate U(phi,t)
energyplusdeltaphi = 0. #variable to calculate U(phi+deltaphi,t)

## Variables
## Set length scale poportional to driver arm length of 1
nD = 3 #number of driver arms
nF = 3 #number of follower arms
rF = 1.0 #follower arm length
h = 0.7 #vertical displacement of follower gear
x = 0.0 #horizontal displacement of follower gear

thetadot = pi #angular velocity of driver
phi0 = pi/10. #initial angule of follower

sigma = 1. #length scale in Lennard Jones energy equation
K = 1. #energy scale in Lennard Jones energy equation
beta = 0.01 #constant in of proportionality in equation phidot = -beta*dU/dphi

gear = 1. #the gear ratio

## Time evolution
## deltat1 and deltat2 are possible time steps
## deltat1 =< deltat2
## Can add more time steps if required
## Conditions which choose the time step are in the loop below
deltat1 = 0.000000005
deltat2 = 0.00005
tmax = 20. #Time at which loop ends





## Create lists which monitor the angles of all driver and follower arms
driverarms = np.zeros(nD)
followerarms = np.zeros(nF)

for i in range(0,nD,1):
    ## Initial angles (theta) of all driver arms
    ## All arms are equally spaced
    ## Theta(t=0) = 0 for the first driver arm
    driverarms[i] = 2.*pi*float(i)/float(nD)
    
for j in range(0,nF,1):
    ## Initial angles (phi) of all follower arms
    ## All arms are equally spaced
    ## phi(t=0) = phi0 for the first driver arm
    followerarms[j] = phi0 + 2.*pi*float(j)/float(nF)
    
## Create some lists which monitor theta and phi
driverrotationangle = [0.] #theta
followerrotationangle = [phi0] #phi





## Now we can set up a loop to let the system evolve
while t<tmax:
    ## Choose the time step for this iteration
    ## In current form, will minimise instability of calculations for small t
    if t<deltat2:
        deltat = deltat1
    else:
        deltat = deltat2
    
    ## Calculate internal energy
    ## Neglecting internal energy between arms on same gear as this will remain constant
    ## Total energy is sum of energies between all arms of driver & follower gears
    ## Calculation updates U(phi,t) and U(phi+deltaphi,t)
    for i in range(0,nD,1):
        for j in range(0,nF,1):
            ## Calculate x,y,z displacement between driver arm i and follower arm j
            xdisplacement = x + rF*np.cos(followerarms[j]) - np.cos(driverarms[i])
            ydisplacement = rF*np.sin(followerarms[j]) - np.sin(driverarms[i])
            zdisplacement = h
            
            ## Calculate distance between driver arm i and follower arm j
            rij = np.sqrt(xdisplacement**2 + ydisplacement**2 + zdisplacement**2)
            
            ## Convert to internal energy between driver arm i and follower arm j
            ## Assume Lennard-Jones potential
            energy += K*((sigma/rij)**12 - (sigma/rij)**6 )
            
            ## Repeat calculations for follower angle changed by deltaphi
            xdisplacementdeltaphi = x + rF*np.cos(followerarms[j]+deltaphi) - np.cos(driverarms[i])
            ydisplacementdeltaphi = rF*np.sin(followerarms[j]+deltaphi) - np.sin(driverarms[i])
            zdisplacementdeltaphi = h
        
            rijdeltaphi = np.sqrt(xdisplacementdeltaphi**2 + ydisplacementdeltaphi**2 + zdisplacementdeltaphi**2)
        
            energyplusdeltaphi += K*((sigma/rijdeltaphi)**12 - (sigma/rijdeltaphi)**6 )
    
    ## Calculate dU/dphi to work out motion of the follower
    dUdphi = (energyplusdeltaphi-energy)/deltaphi
    
    ## Update phidot
    phidot = -beta*dUdphi

    ## Reset U(phi,t) and U(phi+deltaphi,t)
    energy = 0.
    energyplusdeltaphi = 0.
      
    ## Update time
    t += deltat
    
    ## Update angles of all driver arms
    for i in range(0,nD,1):
        driverarms[i] += thetadot*deltat
    
    ## Update theta
    driverrotationangle.append(driverarms[0])
        
    ## Update angles of all follower arms
    for j in range(0,nF,1):
        followerarms[j] += phidot*deltat
        
    ## Update phi
    followerrotationangle.append(followerarms[0])





## Set up a figure to plot results
fig = plt.figure()
ax = fig.add_subplot(111)
plot, = plt.plot(driverrotationangle, followerrotationangle, marker=" ", linestyle= "-", c='b', label='Follower Gear vs Driver Gear')

## Set up a relation (xx,y) to plot the line of best fit
## This will represent the long term motion of the follower
## The gear ratio is the gradient of this line
## If using a notebook version of Python, start a second cell at line 146
## Then change m to alter the gear ratio
xx = np.array([driverrotationangle[0],driverrotationangle[-1]])
m = gear
b = followerrotationangle[0]
y = m*xx + b
lobf, = plt.plot(xx, y, linestyle= "-", c='tab:gray', label='$G = {}$'.format(m))

## Label axes and include a legend
plt.xlabel("Driver Rotation Angle, $\Theta$")
plt.ylabel("Follower Rotation Angle, $\phi$")
plt.legend(handles=[plot,lobf],loc=2)

## Final output of the code
## Follower rotation angle vs driver rotation angle
plt.show()